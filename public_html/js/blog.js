$(function () {
    var APPLICATION_ID = "17A367AB-024C-0917-FF5D-CD46BF042600",
        SECRET_KEY = "03705730-9C8A-8071-FF01-CF5AB585EA00",
        VERSION = "v1";
        
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    
    var postsCollection = Backendless.Persistence.of(Posts).find();
    
    console.log(postsCollection);
    
    var wrapper = {
        posts: postsCollection.data
    };
    
    Handlebars.registerHelper('format', function (time){
        return moment(time).format("dddd, MMMM Do YYYY");
    });
    
    var blogScript = $("#blogs-template").html(); 
    var blogTemplate = Handlebars.compile(blogScript); 
    var blogHTML = blogTemplate(wrapper);
    
    $('.main-container').html(blogHTML); 
    
});  
  
  function Posts(args){
      args = args || {};
      this.title = args.title || "";
      this.content = args.content || "";
      this.authorEmail = args.authorEmail || "";
}